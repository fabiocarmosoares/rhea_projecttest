﻿using Moq;
using NUnit.Framework;
using RHEA_Test;
using RHEA_Test.Interfaces;
using System.Threading.Tasks;

namespace NUnitTestProject
{
    /// <summary>
    /// Test Class
    /// </summary>
    [TestFixture]
    public class UnitTestQ1
    {
        #region Properties

        #region Private
        /// <summary>
        /// Instance of the Tested Class "AccountInfo"
        /// </summary>
        private readonly AccountInfo _accountInfo;

        /// <summary>
        /// Property with the Account Id value Expected for the Test
        /// </summary>
        private readonly int expectedAccountId = 1;

        /// <summary>
        /// Property with the Ammount value Expected in the return of the Test
        /// </summary>
        private readonly double expectedAccountAmount = 100D;

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor Test Class UnitTestQ1
        /// </summary>
        public UnitTestQ1()
        {   
            //mock implementation of service using Moq with expected behavior
            var serviceMock = Mock.Of<IAccountService>(m => m.GetAccountAmount(expectedAccountId) == Task.FromResult(expectedAccountAmount));
            //the system under test                           
            _accountInfo = new AccountInfo(expectedAccountId, serviceMock);
        }

        #endregion

        #region Test Method

        /// <summary>
        /// Test Method: _accountInfo.RefreshAmount();
        /// </summary>
        [Test]        
        public async Task ValidateRefreshAmountAsync()
        {
            //runs method under test
            await _accountInfo.RefreshAmountAsync();

            //Assert

            //verify that expectations have been met           
            Assert.AreEqual(expectedAccountAmount, _accountInfo.Amount);
        }

        #endregion
    }
}