﻿using System.Threading.Tasks;

namespace RHEA_Test.Interfaces
{
    /// <summary>
    /// Interface Declaration IAccountService
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Method to return the Amount in the Account By Account Id.
        /// </summary>
        /// <param name="accountId">Account Id: Code of the Account</param>
        /// <returns>double: Amount Value of the Account.</returns>
        Task<double> GetAccountAmount(int accountId);
    }
}
