﻿using RHEA_Test.Interfaces;
using System.Threading.Tasks;

namespace RHEA_Test
{
    /// <summary>
    /// AccountInfo Implementation Class
    /// </summary>
    public class AccountInfo
    {
        #region Properties

        #region Private

        /// <summary>
        /// Account Id
        /// </summary>
        private readonly int _accountId;

        /// <summary>
        ///Interface IAccountService
        /// </summary>
        private readonly IAccountService _accountService;

        #endregion

        #region Public
                      
        /// <summary>
        /// Amount Parameter
        /// </summary>
        public double Amount { get; private set; }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor Class Method
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="accountService">Interface IAccountService</param>
        public AccountInfo(int accountId, IAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method used to Refresh the Amount in the Account By Account Id.
        /// </summary>
        public async Task RefreshAmountAsync()
        {
            Amount = await _accountService.GetAccountAmount(_accountId);
        }

        #endregion
    }

}
